import React, { useEffect,useState } from 'react'
import { SafeAreaView, ScrollView, StyleSheet, Text, View } from 'react-native'
import { Button, Card, Modal } from 'react-native-paper';
import { useDispatch } from 'react-redux'
import { getBitcoin } from '../actions/bitcoin';
import InfoCoins from './info';

const Home = () => {

    const dispatch = useDispatch();
    const [coins, setCoins] = useState([]);
    const [aux, setAux] = useState('');
    const [bitcoin, setBitcoin] = useState('');

    const showInfo = (coin) => {
        setBitcoin(coin)
        setAux('')
    };

    const hideInfo = (coin) => {
        setAux('bien')
    };
    
    useEffect(() => {
        getBitcoins();
    }, [])

    const getBitcoins = async () =>{
        const response = await dispatch(getBitcoin())
        setCoins(response.coins)
        setAux('bien')
    }
    

    return (
        <SafeAreaView style={styles.container}>
            <ScrollView style={styles.scrollView}>
                {aux !== '' 
                    ?
                    coins.map( index => (
                        <Card>
                            <Card.Title 
                                title={index.name} 
                                subtitle={index.price}/>
                            <Card.Actions>
                                <Button onPress={() => showInfo(index.name)}> ver</Button>
                            </Card.Actions>
                        </Card>
                    ))
                :
                    <>
                        <Card>
                            <Card.Actions> 
                                <Button onPress={() => hideInfo()}>
                                    Atras
                                </Button>
                            </Card.Actions>
                            <InfoCoins coin={bitcoin}/>
                        </Card>
                    </>
                }
            </ScrollView>
        </SafeAreaView>
    )
}

export default Home;


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFF',
    },
    scrollView: {
        marginHorizontal: 20,
    },
    text: {
        fontSize: 42,
    },
    containerStyle: {
        flex: 1,
        backgroundColor: 'white', 
        padding: 20
    },
    view:{
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    }
});
import moment from 'moment';
import React, { useEffect, useState } from 'react'
import { Text, View } from 'react-native'
import { Card } from 'react-native-paper';
import { useDispatch } from 'react-redux';
import { getBitcoinByName } from '../actions/bitcoin';

const InfoCoins = (props) => {

    const dispatch = useDispatch();
    const [info, setInfo] = useState([]);

    useEffect(() => {
        getInfoCoin();
    }, [])

    const getInfoCoin = async () =>{
            const response = await dispatch(getBitcoinByName(props.coin.toLowerCase()));
            setInfo(response.chart);
    }

    return (
        <View>
            <Text style={{fontSize: 42}}>{props.coin}</Text>
            {info.map(index => (
                <Card>
                    <Card.Title 
                        title={moment(index[0]).format('MMMM Do YYYY, h:mm:ss a')} 
                        subtitle={index[1]} />
                </Card>
            ))}
        </View>
    )
}

export default InfoCoins
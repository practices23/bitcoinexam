import React, { useEffect, useState } from 'react'
import { useCallback } from 'react';
import { Alert, Linking, SafeAreaView, ScrollView, StyleSheet, Text, View } from 'react-native'
import { Button, Card } from 'react-native-paper';
import { useDispatch } from 'react-redux';
import { getNews } from '../actions/bitcoin';

const PageNews = () => {

    const dispatch = useDispatch()
    const [news, setNews] = useState([]);
    const [aux, setAux] = useState('');

    useEffect(() => {
        getBitcoins();
    }, [])

    const getBitcoins = async () =>{
            const response = await dispatch(getNews())
            setNews(response.news)
            setAux('bien');
    }
    
    const OpenURLButton = ({ url, children }) => {
        const handlePress = useCallback(async () => {
            const supported = await Linking.canOpenURL(url);
        
            if (supported) {
                await Linking.openURL(url);
            } else {
                Alert.alert(`Don't know how to open this URL: ${url}`);
            }
            }, [url]);
        
            return <Button onPress={handlePress} >{children} </Button>;
    };

    return (
        <SafeAreaView style={styles.container}>
            <ScrollView style={styles.scrollView}>
                {aux !== '' ?
                    news.map(index=> 
                    (
                        <Card>
                            <Card.Cover source={{ uri: index.imgURL }} />
                            <Card.Title 
                                title={index.title} />
                            <Card.Actions>
                                <OpenURLButton url={index.shareURL}>
                                    Ver nota
                                </OpenURLButton>
                            </Card.Actions>
                        </Card>
                    ))
                    :
                    <Text> no se encontraron datos</Text>
                }
            </ScrollView>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFF',
    },
    scrollView: {
        marginHorizontal: 20,
    },
    text: {
        fontSize: 42,
    },
});

export default PageNews
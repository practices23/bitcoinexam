import BitcoinAxios from "../config/axiosBitcoins";
import { API_COINS, API_NEWS, API_SEARCH } from "../utils/yrlWebServices";


/**
 * Funcion para obtener bitcoins
 * 
 * @param {*} data 
 */
export function getBitcoin(){

    return async (dispatch) => {

        try {
            
            const response = await BitcoinAxios.get(API_COINS);

            if( response.status === 200 ){
                return response.data;
            }else{
                return {
                    code:500,
                    message: 'error al obtener bitcoins', 
                    severity: 'error'
                };
            } 
        } catch (error) {
            return {
                code: 500,
                message: 'error al obtener bitcoins',
                severity: 'error'
            }
        };
    }
}


/**
 * Funcion para obtener bitcoins
 * 
 * @param {*} data 
 */
export function getBitcoinByName(name){

    return async (dispatch) => {

        try {
            
            const response = await BitcoinAxios.get(`${API_SEARCH}${name}`);

            if( response.status === 200 ){
                return response.data;
            }else{
                return {
                    code:500,
                    message: 'error al obtener bitcoins', 
                    severity: 'error'
                };
            } 
        } catch (error) {
            return {
                code: 500,
                message: 'error al obtener bitcoins',
                severity: 'error'
            }
        };
    }
}


/**
 * Funcion para obtener bitcoins
 * 
 * @param {*} data 
 */
export function getNews(){

    return async (dispatch) => {

        try {
            
            const response = await BitcoinAxios.get(API_NEWS);

            if( response.status === 200 ){
                return response.data;
            }else{
                return {
                    code:500,
                    message: 'error al obtener bitcoins', 
                    severity: 'error'
                };
            } 
        } catch (error) {
            return {
                code: 500,
                message: 'error al obtener bitcoins',
                severity: 'error'
            }
        };
    }
}

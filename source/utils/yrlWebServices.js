export const API_COINS  = '/public/v1/coins?skip=0&limit=20&currency=USD'
export const API_SEARCH = '/public/v1/charts?period=1m&coinId='
export const API_NEWS   ='/public/v1/news/latest?skip=0&limit=20'

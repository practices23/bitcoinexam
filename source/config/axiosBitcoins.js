import axios from 'axios'

const BitcoinAxios = axios.create({
    baseURL:'https://api.coinstats.app/'
})


export default BitcoinAxios
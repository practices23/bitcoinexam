import { combineReducers } from 'redux';
import dataReducer from './initialRenderData/dataReducer';

export default combineReducers({
    dataReducer,
});